# Spring Security

- Biasanya kita menggunakan OAuth
- begitu dia mau mengakses, maka dia harus ambil token dulu 
- kemudian dia mendapatkan akses token
- nak akses token tersebut dipakai buat request selanjutnya

- Jaman sekarang aplikasi bisnis selalu ada 2, yakni Mobile dan Website
- oleh karena itu, kita bisa langsung bikin Login dengan benar, OAuth misalnya
- Intinya bahwa aplikasi kita menyediakan login pake web biasa (ada login formnya),
- tapi juga ada login buat mobile
- Login->dapat token->tokennya dibuat untuk mengakses API

- Kita punya API Server itu namanya `Resource Server`, jadi menyediakan API
- Kemudian API kita akan di-consume oleh aplikasi lain, disebut dengan `Client Apps`
- Client App ini bisa mengakses banyak resource server
- Nah untuk mengakses ke backend, maka Client App butuh `token`
- Akses token ini yang menerbitkan adalah `Authorization Server`
- Jadi ada entity lagi bernama Authorization Server, dia yang memegang username dan password
- Kalau ada orang mau login, maka loginnya ke dia
- setelah selesai login, User akan dapat token. Nah tokenya itu yang akan digunakan User untuk mengakses ke API
- Pada praktiknya jika aplikasi kita adalah `Monolith`, maka kita menggabungkan aplikasi kita dengan AuthServer
- aplikasi kita juga berfungsi sebagai AuthServer
- Contohnya kalau aplikasi kita kecil, maka kita gabungkan AuthServer dengan aplikasi utama kita

- kita mau username dan password itu adanya di database
- biasanya kita punya skema database sendiri untuk username dan password
- ditambah biasanya ada Permission (role)
- biasanya data password dijadikan table tersendiri (pisah dengan data username)
- karena jika data username di ambil sebagai response JSON, maka passwordnya tidak akan ikut

## Csrf

- Csrf Filter digunakan untuk mencegah security issue yang bernama `Csrf` 
- misalnya kita buka website A, kemudian website A ini ada form inputnya
- kemudian kita isi formnya lalu klik submit
- sesudah itu harapannya kan sukses
- karena ini formnya website A tapi kita bukanya di browser dengan website A juga
- Nah yang jadi issue adalah si request ini ditangkap orang
- artinya dia tau nama-nama variablenya kemudian dia bikin javascript di website B misalnya
- yang kalau kita bukan website si B ini, maka seolah-olah dia bisa mengirim request ke website A dengan seolah-olah kita ngisi form (padahal ini di website B)
- Misalnya di website A kita melakukan transfer uang di internet banking
- kemudian kita buka website B ternyata website B ini mengirim request ke website A tadi untuk melalukan transfer juga, padahal di beda website
- Nah untuk mencegahnya, si website A ini waktu dia menampilkan form, dia akan menampilkan juga Token (akan mengenerate random token)
- Nah tokennya itu kemudian akan ditampilkan di dalam formnya
- sehingga waktu kita mengisi form lalu kita submit, maka si nilai token itu akan ikut disubmit
- Si server atau aplikasi kita akan ngecek. Apakah token yang disubmit tadi sama dengan token yang ia generate
- sehingga saat kita melakukan transfer di website A, selain mengirim informasi transfer kita juga mengirim token
- Nah di website B karena dia tidak merender formnya, maka dia tidak tau nilai tokennya berapa
- sehingga kalo si website B request dan requestnya tidak menyertakan token
- maka akan ditolak oleh Aplikasi A
- Intinya aplikasi/website A tadi bisa membedakan request mana yang benar-benar datang dari form yang ia tampilkan dan mana request yang datang dari form yang tidak ia tampilkan
- caranya menggunakan csrf token
- CsrfFilter punyanya Spring Security, tugasnya ngecek apakah token yang ikut disubmit itu sama atau tidak 
- Kapan menggunakan Csrf? Csrf ini biasanya kita gunakan tokennya atau proteksi ini kalo URL yang mau kita akses itu, diaksesnya lewat form html
- Nah disini kita menggunakan API, API itu biasanya tidak di render di browser
- makanya untuk API kita ingin mematikan Csrf filter, sedangkan untuk HTML yang dirender di browser kita tetap mengaktifkan Csrf filternya