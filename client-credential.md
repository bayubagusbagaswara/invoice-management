# Client Credential

- bedanya dengan tadi ada username+passwordnya
- Client Credential tidak melibatkan user (tidak perlu data user)
- hanya koneksi dari Service ke Service saja

1. Client -> Auth Server
- grant : client credential
- client id
- secret
2. Auth Server -> Client
- access token
- refresh token
3. Client -> Resource Server
- access token
4. Check di Resource
Cek secara mandiri di Resource saja
- value token
- atau sekarang umumnya JWT
Cek ke Auth Server
Dari cek akan dapat Token Info(pemilik tokennya, scopenya, expired tokennya)


## Device
- untuk menghandle seperti kita punya televisi, punya kamera, yang ingin simpan data di cloud
- si device gak bisa login sendiri
- kalau dia gak punya browser didalamnya, misal kamera tidak punya browser
- entah membuka browsernya di handphone atau dilaptop

Flownya
1. User -> Client App
- user mengirim request (start)
2. Client -> Auth Server
- dia akan bawa client id dan scope
3. reply Auth Server -> Client 
- device code
- expired
- interval
- url
- user code

4. Display di layar
- code
- url
- Silahkan buka url ini untuk melanjutkan ...... masukkan kode berikut dibrowser
5. User buka URL di browser
- input code 
6. mendapatkan Access Token dan Refresh Token
ada 3 kemungkinan:
- Pending (usernya belum input kode di browser)
- Denied (tidak boleh)
- OK (mendapatkan access token dan refresh token)
7. Client bisa akses ke Resource Server membawa access Token
8. a
9. sd
10. a
11. sd