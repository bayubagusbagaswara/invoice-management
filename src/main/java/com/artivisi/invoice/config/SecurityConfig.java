package com.artivisi.invoice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;

/**
 * harus extends WebSecurityConfigurerAdapter
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    // karena kita Skema databasenya kustom, kita harus sediakan 2 sql
    // sql pertama untuk login, login menggunakan username, maka kita akan query passwordnya
    // sql kedua untuk permissions, kalo kita punya username, maka kita ingin mendapatkan permission dari username tersebut apa saja
    private static final String SQL_LOGIN
            = "select u.username, up.password, u.active " +
            "from s_users_passwords up " +
            "inner join s_users u on u.id = up.id_user " +
            "where u.username = ?";

    private static final String SQL_PERMISSION
            = "select u.username, p.value as authority " +
            "from s_users u " +
            "inner join s_roles r on r.id = u.id_role " +
            "inner join s_roles_permissions rp on rp.id_role = r.id " +
            "inner join s_permissions p on rp.id_permission = p.id " +
            "where u.username = ?";

    // karena kita query langsung ke database, maka kita inject Data Source
    @Autowired
    private DataSource dataSource;

    // Bean ini yang menyediakan password encoder
    // kita instankan algoritma BCrypt
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // kita konfigurasi UserDetailService
    // kita mau mendapatkan username dan password dari aplikasi kita
    // karena kita mau mengakses database, maka kita menginstankan JdbcUserDetailsManager

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        // memerlukan input koneksi databasenya apa?
        // querynya untuk login apa?
        // untuk mencari permission apa?
        JdbcUserDetailsManager manager = new JdbcUserDetailsManager(this.dataSource);
        manager.setUsersByUsernameQuery(SQL_LOGIN);
        manager.setAuthoritiesByUsernameQuery(SQL_PERMISSION);
        return manager;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService())
                // passwordEncodernya kita pake yang atas
                .passwordEncoder(passwordEncoder());
    }

    // fully authenticated, artinya semua url yang masuk harus diautentikasi
    // saat ini kita belum mau setting seperti itu
    // kita ingin bedakan antara API dengan HTML code yang kita akses lewat browser
    // caranya yakni kita bikin 2 konfigurasi
    // konfigurasi pertama yaitu untuk yang lebih spesifik dulu
    // konfigurasi kedua itu untuk yang lebih umum
    // jadi ia akan coba yang pertama dulu, jika yang pertama gagal, maka dia akan diarahkan ke yang kedua
    // jadi yang akan menghandle semuanya itu kita taruh belakangan
    // dan yang untuk API kita taruh duluan
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().anyRequest().fullyAuthenticated();
//    }

    // bikin class lagi untuk konfigurasi, dan tambahkan anotasi @Configuration
    @Configuration @Order(1)
    static class ApiSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // kita mau semuanya itu diijinkan
            // kita hanya menghandle satu pattern tertentu
            // artinya ini semua yang request ke API, kita tidak butuh autentikasi, jadi di permitAll
            // kita matikan Csrf Filter
            http.antMatcher("/api/**")
                    .authorizeRequests
                            (authorize -> authorize
                                    .anyRequest()
                                    .permitAll())
                    .csrf()
                    .disable()
            ;
        }
    }

    // kita bikin satu configuration lagi
    // konfigurasi untuk autentikasi semua
    // dihalaman browser kita tetap perlu csrf filter. Oleh karena itu, jangan mematikan csrf filter
    @Configuration @Order(2)
    static class HtmlSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .authorizeRequests(authorize -> authorize
                            // bisa menuliskan secure untuk authority disini
                            // misal untuk URL /home maka authority harus VIEW_TRANSAKSI
                            // bisa lewat sini, atau bisa lewat Annotation di Controller
//                            .antMatchers("/home").hasAuthority("VIEW_TRANSAKSI")
                            .anyRequest().authenticated())
                    .formLogin(Customizer.withDefaults())
            ;
        }
    }

    // Authorization, sesudah mendapat username, lalu lihat ijin akses apa yang didapatkan oleh user tersebut
}
