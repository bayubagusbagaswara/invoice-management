package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.VirtualAccount;
import com.artivisi.invoice.entity.VirtualAccountConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VirtualAccountConfigurationRepository extends JpaRepository<VirtualAccountConfiguration, String> {
//    Optional<VirtualAccountConfiguration> findByVirtualAccountConfigurationAndAccountNumber(VirtualAccountConfiguration config, String accountNumber);

}
