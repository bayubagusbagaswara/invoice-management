package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.ActivityLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityLogRepository extends JpaRepository<ActivityLog, String> {
}
