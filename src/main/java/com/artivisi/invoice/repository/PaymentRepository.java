package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.Payment;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PaymentRepository extends PagingAndSortingRepository<Payment, String> {
}
