package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.Invoice;
import com.artivisi.invoice.entity.PaymentProvider;
import com.artivisi.invoice.entity.VirtualAccount;
import com.artivisi.invoice.entity.VirtualAccountConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface VirtualAccountRepository extends JpaRepository<VirtualAccount, String> {

    Optional<VirtualAccount> findByVirtualAccountConfigurationAndAccountNumber(VirtualAccountConfiguration config, String accountNumber);
    

    Optional<VirtualAccount> findByPaymentProviderAndCompanyIdAndAccountNumber(PaymentProvider provider, String companyId, String accountNumber);

    Iterable<VirtualAccount> findByInvoice(Invoice invoice);
}
