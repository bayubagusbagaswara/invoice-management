package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, String> {

    Optional<Customer> findByCode(String customerCode);
}
