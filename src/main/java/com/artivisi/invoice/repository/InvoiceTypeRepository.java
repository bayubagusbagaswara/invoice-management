package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.InvoiceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface InvoiceTypeRepository extends JpaRepository<InvoiceType, String> {
    Optional<InvoiceType> findByCode(String invoiceTypeCode);
}
