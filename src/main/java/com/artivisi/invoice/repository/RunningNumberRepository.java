package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.RunningNumber;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;

public interface RunningNumberRepository extends CrudRepository<RunningNumber, String> {

     /**
      * DB Locking itu ada 2, optimistic dan pesimistic
      * Optimistic : optimis bahwa tidak akan terjadi konflik (contoh ada nomor yang duplikat). Kita optimis bahwa tidak akan terjadi orang mengakses record itu secara bebarengan. Jadi kita tidak me-lock databasenya
      * Untuk mencegah konflik dari Optimistic, maka kita lakukan di aplikasi, atau kita cek konfliknya di aplikasi
      * cara mengecek konflik di aplikasi dengan cara menambahkan kolom version
      * Pesimistic : lock di Database, siapapun yang konek ke database maka ida akan ikut kena locknya
      *
      */
     @Lock(LockModeType.PESSIMISTIC_WRITE)
     RunningNumber findByPrefix(String prefix);
}
