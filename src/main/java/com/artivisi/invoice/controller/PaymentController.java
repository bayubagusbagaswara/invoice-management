package com.artivisi.invoice.controller;

import com.artivisi.invoice.exception.PaymentExceedInvoiceAmountException;
import com.artivisi.invoice.exception.VirtualAccountAlreadyPaidException;
import com.artivisi.invoice.exception.VirtualAccountNotFoundException;
import com.artivisi.invoice.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired private PaymentService paymentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void pay() throws VirtualAccountAlreadyPaidException, VirtualAccountNotFoundException, PaymentExceedInvoiceAmountException {
        /**
         * begitu di menjalankan method pay,
         * dia akan memanggil si paymentService method pay
         */
        paymentService.pay(
                null,
                null,
                null,
                null,
                null
        );
    }
}
