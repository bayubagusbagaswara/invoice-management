package com.artivisi.invoice.controller;

import com.artivisi.invoice.dto.CreateInvoiceRequestDto;
import com.artivisi.invoice.entity.Customer;
import com.artivisi.invoice.entity.InvoiceType;
import com.artivisi.invoice.exception.CustomerNotFoundException;
import com.artivisi.invoice.exception.InvoiceTypeNotFoundException;
import com.artivisi.invoice.repository.CustomerRepository;
import com.artivisi.invoice.repository.InvoiceTypeRepository;
import com.artivisi.invoice.service.InvoiceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * biasanya REST API kan input terimanya adalah JSON dan keluarannya JSON juga
 * Menambah data baru menggunakan method POST, karena database akan ada data baru
 * method GET untuk mengambil data di database, dan tidak mengubah data didalamnya
 * Beda POST dan PUT, adalah POST tidak bisa dijalankan 2 kali, artinya dia akan insert data 2 kali
 * kalo PUT kita jalanin berulang kali, maka hasilnya tetap sama
 * Idempoten artinya dijalankan berkali-kali tapi hasilnya tetap sama, yakni GET PUT
 * sedangkan post tidak, karena hasilnya bisa berubah tiap kali dijalankan
 */
@Slf4j
@RestController
@RequestMapping("/api/invoice")
public class InvoiceApiController {

    @Autowired private CustomerRepository customerRepository;
    @Autowired private InvoiceTypeRepository invoiceTypeRepository;
    @Autowired private InvoiceService invoiceService;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    // kita validasi juga semua properti dari DTO, gunakan @Valid
    // kita bisa menginject untuk Authentication
    // currentUser ini bisa diisi object user yang sedang login
    // object user itu bisa macem-macem
    // kalau kita login via web, maka kita akan mendapatkan UsernamePasswordAuthenticationToken
    // lalu kita cek berdasarkan UsernamePasswordAuthenticationToken tersebut

    public void createInvoice(
            Authentication currentUser,
            @RequestBody @Valid CreateInvoiceRequestDto createInvoiceRequestDto) throws CustomerNotFoundException, InvoiceTypeNotFoundException {

        // cek jenis authentication yang digunakan user
        // jika currentUser loginnya membawa UsernamePasswordAuthenticationToken maka di login via HTML Form di browser
        if (UsernamePasswordAuthenticationToken.class.isAssignableFrom(currentUser.getClass())){
            // berarti dia login lewat HTML Form
            // kita hanya bisa membedakan apakah dia login via HTML form atau via API
            // kita tidak membedakan HTML form ini dibuka lewat Laptop atau lewat Gadget
            currentUser.getAuthorities(); // ini isinya nanti adalah data Permission untuk usernya
            // prosesnya akan diteriman oleh URL /LOGIN, lalu dia akan menjalankan query untuk loginnya
            // lalu cek username dan password
            // setelah mendapatkan username dan password, dia akan menjalankan query Permission (berdasarkan username)
            // lalu mendapatkan data Permission
            // data Permission nya akan dimasukkan kedalam object getAuthorities()
            // setelah terisi, maka objectnya bisa dipake di anotasi @PreAuthorize (pada method di Controller)
            // @PreAuthorize sama @Secure sama saja fuungsinya
        }

        // jika currentUser loginnya membawa BearerTokenAuthentication, maka dia Login lewat API OAuth
        if (BearerTokenAuthentication.class.isAssignableFrom(currentUser.getClass())) {
            // kalau jenis autentikasinya adalah BearerToken
            // berarti dia login lewat API OAuth/OpenID
            // saat login membawa Bearer, maka getAuthorities nya tidak keisi
            // sehingga kita tidak bisa menjalankan @PreAuthorize di controller
            // lalu gimana solusinya?
            // harusnya kalau aplikasi kita dengan server yang menerbitkan token nya adalah Spring-to-Spring
            // maka biasanya dia sudah langsung mengisi authority (harus tidak kosong)
            // kalau sama-sama menggunakan UserDetailService, harusnya sudah keisi
            // atau misal login lewat SSO dengan Google
            // seperti login with Facebook, login with Google (pihak ketiga yang bukan spring)
            // dia didalam Tokennya tidak membawa data authority, ke kita itu ya null authority nya
            // caranya adalah kita harus query sendiri
            // waktu login kan kita lempar ke sana, balik lagi dia bawa token
            // aplikasi kita kenalnya hanya username nya saja
            // lalu kita harus query lagi ke table permission kita ini, cara permission untuk username tersebut
            // lalu kita masukkan data permission nya kedalam object Authentication nya
            // sehingga object Authenticationnya waktu diperiksa di Controller (@PreAuthorize) dia sudah memiliki authority nya

        }

        // kita cari satu-satu customernya
        Customer customer = customerRepository.findByCode(createInvoiceRequestDto.getCustomerCode())
                .orElseThrow(() -> new CustomerNotFoundException("Customer code " +createInvoiceRequestDto.getCustomerCode()+ " not found"));
        log.info("Customer {}", customer.getName());

        // ambil invoice type
        InvoiceType invoiceType = invoiceTypeRepository.findByCode(createInvoiceRequestDto.getInvoiceTypeCode())
                .orElseThrow(() -> new InvoiceTypeNotFoundException("Invoice type " +createInvoiceRequestDto.getInvoiceTypeCode()+ " not found"));
        log.info("Invoice Type {}", invoiceType.getName());

        // create invoice
        invoiceService.createInvoice(
                customer,
                invoiceType,
                createInvoiceRequestDto.getDescription(),
                createInvoiceRequestDto.getAmount()
        );

        // kita mapping menjadi response code (Error Handling for REST)
        log.info("Invoice created");
    }
}
