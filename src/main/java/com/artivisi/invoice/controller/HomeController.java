package com.artivisi.invoice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
public class HomeController {

    // otoritas tiep user, user memiliki authority apa saja, misal "EDIT_TRANSAKSI"
//    @Secured("hasRole('ROLE_EDIT_TRANSAKSI')")
    // @PreAuthorize bisa kita pindah di form HTML, jadi pengecekan Authority nya dilakukan di form HTML
    @PreAuthorize("hashAnyAuthority('EDIT_TRANSAKSI', 'EDIT_TRANSAKSI')")
    @GetMapping("/home")
    public void home(Authentication userYangSedangLogin) {

        // kita bisa menampilkan si user loginnya lewat mana, apakah HTML form atau API
        log.info("Jenis class authentication : {}", userYangSedangLogin.getClass().getSigners());
        // kita bisa menampilkan User yang sedang login
        log.info("User yang sedang login : {}", userYangSedangLogin.getPrincipal()); // usernya siapa

        User currentUser = (User) userYangSedangLogin.getPrincipal();



    }
}
