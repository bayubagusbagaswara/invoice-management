package com.artivisi.invoice.service;

import com.artivisi.invoice.entity.Customer;
import com.artivisi.invoice.entity.Invoice;
import com.artivisi.invoice.entity.InvoiceType;

import java.math.BigDecimal;

public interface InvoiceService {

    Invoice createInvoice(Customer customer, InvoiceType invoiceType, String description, BigDecimal amount);

}
