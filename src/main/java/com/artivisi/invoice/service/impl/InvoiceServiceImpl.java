package com.artivisi.invoice.service.impl;

import com.artivisi.invoice.entity.*;
import com.artivisi.invoice.helper.InvoiceHelper;
import com.artivisi.invoice.repository.InvoiceRepository;
import com.artivisi.invoice.repository.InvoiceTypeRepository;
import com.artivisi.invoice.repository.VirtualAccountRepository;
import com.artivisi.invoice.service.InvoiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;

@Service
@Slf4j
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired private InvoiceHelper invoiceHelper;
    @Autowired private InvoiceRepository invoiceRepository;
    @Autowired private InvoiceTypeRepository invoiceTypeRepository;
    @Autowired private VirtualAccountRepository virtualAccountRepository;

        // buat client code, code program yang memanggil method createInvoice ini
        // paling gampang bikinnya di test class, cuma ini berbeda dengan test driven development (TDD)
        // karena test itu adalah methode untuk memvalidasi apakah yg kita kerjakan apakah benar atau salah
        // menggunakan test untuk mendesain kode program

    @Override
    public Invoice createInvoice(Customer customer, InvoiceType invoiceType, String description, BigDecimal amount) {

        InvoiceType it = invoiceTypeRepository.findById(invoiceType.getId())
                .orElseThrow(() -> new IllegalStateException("Invoice type " + invoiceType.getId() + " not found "));

        Invoice invoice = new Invoice();
        invoice.setInvoiceNumber(invoiceHelper.generateInvoiceNumber());
        invoice.setCustomer(customer);
        invoice.setInvoiceType(it);
        invoice.setDescription(description);
        invoice.setAmount(amount);
        invoice.setDueDate(LocalDate.now().plusMonths(1));
        invoiceRepository.save(invoice);

        log.info("[INVOICE]-[CREATED] : {}|{}|{}",
                invoice.getInvoiceType().getCode(),
                invoice.getInvoiceNumber(),
                invoice.getCustomer().getCode());

        for (VirtualAccountConfiguration vaConfig : it.getInvoiceTypeConfiguration()) {
            VirtualAccount virtualAccount = new VirtualAccount();
            virtualAccount.setVirtualAccountConfiguration(vaConfig);
            virtualAccount.setInvoice(invoice);
            virtualAccount.setAccountNumber(vaConfig.getCompanyPrefix() + invoice.getCustomer().getCode());
            virtualAccountRepository.save(virtualAccount);
        }
        return invoice;
    }
}
