package com.artivisi.invoice.service.impl;

import com.artivisi.invoice.entity.ActivityLog;
import com.artivisi.invoice.entity.Feature;
import com.artivisi.invoice.repository.ActivityLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tujuan AuditLog menggunakan propagation Required New
 * dimana dia melalukan log aktifitas terhadap transaksi di service lain
 * dan transasksi tidak tergantung jika terjadi error atau rollback pada transaksi service lain
 */
@Service
public class ActivityLogServiceImpl {

    @Autowired private ActivityLogRepository activityLogRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void log(String logMessage) {

        // suspend transaction yang sedang berjalan (tx1)
        // start transaction baru yakni (tx2)
        // program yang ada disini akan dijalankan didalam transaction baru (tx2)

        ActivityLog activityLog = new ActivityLog();
        activityLog.setFeature(Feature.PAYMENT);
        activityLog.setMessage(logMessage);
        activityLogRepository.save(activityLog);

        // commit atau rollback tx2
        // lanjutkan transaction tx1

    }
}
