package com.artivisi.invoice.service.impl;

import com.artivisi.invoice.entity.Invoice;
import com.artivisi.invoice.entity.StatusRecord;
import com.artivisi.invoice.exception.PaymentExceedInvoiceAmountException;
import com.artivisi.invoice.repository.InvoiceRepository;
import com.artivisi.invoice.repository.VirtualAccountRepository;
import com.artivisi.invoice.entity.PaymentProvider;
import com.artivisi.invoice.entity.VirtualAccount;
import com.artivisi.invoice.exception.VirtualAccountAlreadyPaidException;
import com.artivisi.invoice.exception.VirtualAccountNotFoundException;
import com.artivisi.invoice.helper.VirtualAccountHelper;
import com.artivisi.invoice.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.transaction.TransactionScoped;
import java.math.BigDecimal;

@Service
@Transactional(
        propagation = Propagation.REQUIRED,
        rollbackFor = {
                VirtualAccountNotFoundException.class,
                VirtualAccountAlreadyPaidException.class,
                PaymentExceedInvoiceAmountException.class
        }
)
public class PaymentServiceImpl implements PaymentService {

     private final ActivityLogServiceImpl activityLogService;
     private final VirtualAccountRepository virtualAccountRepository;
     private final VirtualAccountHelper virtualAccountHelper;
     private final InvoiceRepository invoiceRepository;

     @Autowired
    public PaymentServiceImpl(ActivityLogServiceImpl activityLogService,
                              VirtualAccountRepository virtualAccountRepository,
                              VirtualAccountHelper virtualAccountHelper,
                              InvoiceRepository invoiceRepository) {
        this.activityLogService = activityLogService;
        this.virtualAccountRepository = virtualAccountRepository;
        this.virtualAccountHelper = virtualAccountHelper;
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    @Transactional(
        rollbackFor = {
                VirtualAccountNotFoundException.class,
                VirtualAccountAlreadyPaidException.class,
                PaymentExceedInvoiceAmountException.class
        })
    public void pay(PaymentProvider provider, String companyId, String accountNumber, BigDecimal amount, String reference)
            throws VirtualAccountNotFoundException, VirtualAccountAlreadyPaidException, PaymentExceedInvoiceAmountException {

        // begin tx1
        // memanggil AuditLogService untuk melakukan log transaksi service pay ini
        // disini dia akan suspend Tx1 dalam method log
        activityLogService.log("Start payment VA " + accountNumber);

        // 1. cek apakah VA ada atau tidak?
        VirtualAccount virtualAccount = virtualAccountHelper.getExistingVirtualAccount(provider, companyId, accountNumber);

        // 2. cek apakah VA sudah lunas atau belum?
        virtualAccountHelper.checkVaAlreadyPaid(virtualAccount);

        // 3. cek apakah amount pembayaran > nilai tagihan
        virtualAccountHelper.checkPaymentAmount(virtualAccount, amount);

        // 4. update status VA menjadi lunas
        virtualAccount.setStatusRecord(StatusRecord.INACTIVE);

        // 5. update status invoice menjadi lunas (FULL)
        Invoice invoice = virtualAccount.getInvoice();
        // set total pembayaran di invoice
        invoice.setTotalPayment(invoice.getTotalPayment().add(amount));
        // invoice sudah terbayar
        invoice.setPaymentStatus(Invoice.PaymentStatus.FULL);
        // save invoice
        invoiceRepository.save(invoice);

        virtualAccountRepository.findByInvoice(invoice);

        // 6. insert ke table payment
        // 7. notifikasi (next fase)
        // commit tx1

//        throw new VirtualAccountAlreadyPaidException("Invoice sudah dibayar");
    }

    @Override
    public void undoPayment() {}

}
