package com.artivisi.invoice.service.impl;

import com.artivisi.invoice.entity.RunningNumber;
import com.artivisi.invoice.repository.RunningNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RunningNumberServiceImpl {

    @Autowired private RunningNumberRepository runningNumberRepository;

    // dengan angka atau prefix kita akan mendapatkan nomor yang terbaru
    @Transactional
    public Long getNumber(String prefix) {
        RunningNumber runningNumber = runningNumberRepository.findByPrefix(prefix);

        // cek jika dia null
        if (runningNumber == null){
            // maka buatkan number baru
            runningNumber = new RunningNumber();
            // set prefix numbernya
            runningNumber.setPrefix(prefix);
        }
        // jika number sebelumnya sudah ada, maka ambil dan tambahkan 1
        runningNumber.setLastNumber(runningNumber.getLastNumber() + 1);
        // lalu save
        runningNumberRepository.save(runningNumber);

        return runningNumber.getLastNumber();
    }

}
