package com.artivisi.invoice.service;

import com.artivisi.invoice.entity.PaymentProvider;
import com.artivisi.invoice.entity.VirtualAccount;
import com.artivisi.invoice.exception.PaymentExceedInvoiceAmountException;
import com.artivisi.invoice.exception.VirtualAccountAlreadyPaidException;
import com.artivisi.invoice.exception.VirtualAccountNotFoundException;

import java.math.BigDecimal;

public interface PaymentService {

    // yang dibayarkan
    void pay(PaymentProvider provider, String companyId, String accountNumber, BigDecimal amount, String reference) throws VirtualAccountNotFoundException, VirtualAccountAlreadyPaidException, PaymentExceedInvoiceAmountException;

    void undoPayment();

}
