package com.artivisi.invoice.entity;

public enum PaymentType {
    CLOSED, OPEN, INSTALLMENT
}
