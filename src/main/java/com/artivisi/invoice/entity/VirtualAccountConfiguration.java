package com.artivisi.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE virtual_account_configuration SET status_record = 'INACTIVE' WHERE id=?")
@Where(clause = "status_record='ACTIVE'")
public class VirtualAccountConfiguration extends BaseEntity {

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 100)
    private String code;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 100)
    private String name;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_payment_provider")
    private PaymentProvider paymentProvider;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_bank_account")
    private BankAccount bankAccount;

    @NotNull
    @Min(0)
    private BigDecimal transactionFeeFlat = BigDecimal.ZERO;

    @NotNull
    @Min(0)
    private BigDecimal transactionFeePercentage = BigDecimal.ZERO;

    @NotNull
    @NotEmpty
    private String companyPrefix;

    @NotNull
    @Min(0)
    private Integer accountNumberLength;

    public Integer totalAccountNumberLength() {
        return accountNumberLength + companyPrefix.length();
    }
}
