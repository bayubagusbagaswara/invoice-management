package com.artivisi.invoice.exception;

// RuntimeException dipakai untuk error yang yang tidak bisa dihandle oleh yang panggil contohnya Controller API, controller Web, ISO8583 handler
// contoh: misal ada query select one, tapi balikanya lebih dari satu
// jangan pakai runtime exception, supaya wajib dihandle oleh yang panggil
public class VirtualAccountAlreadyPaidException extends Exception {

    public VirtualAccountAlreadyPaidException() {
    }

    public VirtualAccountAlreadyPaidException(String message) {
        super(message);
    }
}
