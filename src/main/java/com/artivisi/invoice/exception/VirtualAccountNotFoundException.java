package com.artivisi.invoice.exception;

// checked exception
// supaya wajib dihandle oleh yang panggil
public class VirtualAccountNotFoundException extends Exception {
    public VirtualAccountNotFoundException() {
    }

    public VirtualAccountNotFoundException(String message) {
        super(message);
    }
}
