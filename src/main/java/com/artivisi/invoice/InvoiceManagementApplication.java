package com.artivisi.invoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.math.BigInteger;

@SpringBootApplication
public class InvoiceManagementApplication {

	public static void main(String[] args) {

		SpringApplication.run(InvoiceManagementApplication.class, args);

		// cek apakah amount pembayaran (jumlah pembayaran) > nilai tagihan
		// jika jumlah pembayaran < nilai tagihan, maka error
		// x < y = -1 true
		// x > y = erro
//		int jumlahPembayaran = 10;
//		int nilaiTagihan = 11;
//		BigDecimal nilaiTagihan = new BigDecimal(BigInteger.valueOf(10000L));
//		BigDecimal jumlahPembayaran = new BigDecimal(BigInteger.valueOf(20000L));
//
//		if (nilaiTagihan.compareTo(jumlahPembayaran) <= 0) {
//			System.out.println("Jumlah Pembayaran: " + jumlahPembayaran);
//			System.out.println("Nilai Tagihan: " + nilaiTagihan);
//			System.out.println("error");
//		} else if (nilaiTagihan.compareTo(jumlahPembayaran) > 0) {
//			System.out.println("Pembayaran berhasil");
//		}
//
//		else {
//			System.out.println("default");
//		}
	}
}
