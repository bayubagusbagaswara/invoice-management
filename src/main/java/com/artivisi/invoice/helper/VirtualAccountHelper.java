package com.artivisi.invoice.helper;

import com.artivisi.invoice.exception.PaymentExceedInvoiceAmountException;
import com.artivisi.invoice.exception.VirtualAccountAlreadyPaidException;
import com.artivisi.invoice.repository.VirtualAccountRepository;
import com.artivisi.invoice.entity.PaymentProvider;
import com.artivisi.invoice.entity.VirtualAccount;
import com.artivisi.invoice.exception.VirtualAccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

// yang menginstankan autowired adalah spring, maka tambahkan @Component
@Component
public class VirtualAccountHelper {

    @Autowired private VirtualAccountRepository virtualAccountRepository;

    public VirtualAccount getExistingVirtualAccount(
            PaymentProvider provider,
            String companyId,
            String accountNumber)
            throws VirtualAccountNotFoundException {
        Optional<VirtualAccount> optionalVirtualAccount = virtualAccountRepository.findByPaymentProviderAndCompanyIdAndAccountNumber(provider, companyId, accountNumber);

        if (!optionalVirtualAccount.isPresent()) {
            throw new VirtualAccountNotFoundException("VA ["+ companyId +"/"+ accountNumber +"-"+ provider.getCode()+"] not found");
        }
        return optionalVirtualAccount.get();
    }

    public void checkVaAlreadyPaid(VirtualAccount virtualAccount) throws VirtualAccountAlreadyPaidException {
        if (virtualAccount.getInvoice().getPaid()) {
            throw new VirtualAccountAlreadyPaidException("VA ["+ virtualAccount.getCompanyId() +"/"+ virtualAccount.getAccountNumber() +"-"+ virtualAccount.getPaymentProvider().getCode() + "] already paid");
        }
    }

    public void checkPaymentAmount(VirtualAccount virtualAccount, BigDecimal amount) throws PaymentExceedInvoiceAmountException {
        // cek apakah amount pembayaran > nilai tagihan
//        if (virtualAccount.getInvoice().getAmount().compareTo(amount) >= 0) {
//            throw new PaymentExceedInvoiceAmountException("Total Pembayaran : " + amount + ", Nilai Tagihan : " + virtualAccount.getInvoice().getAmount());
//
//        }
    }

}
