package com.artivisi.invoice.helper;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class InvoiceHelper {

    public String generateInvoiceNumber() {
        return ""+LocalDateTime.now().getYear()+""+LocalDateTime.now().getMonthValue()+""+LocalDateTime.now().getDayOfMonth()
                +""+LocalDateTime.now().getHour()+""+LocalDateTime.now().getMinute()+""+LocalDateTime.now().getSecond();
    }
}
