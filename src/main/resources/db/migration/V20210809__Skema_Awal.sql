
-- ACTIVITY LOG
create table activity_log (
    id varchar(36) not null,
    activity_time datetime not null,
    feature varchar(255) not null,
    message varchar(255) not null
);

alter table activity_log
    add constraint activity_log_pkey primary key (id);

-- BANK
create table bank (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    code varchar(100) not null,
    name varchar(100) not null,
    central_bank_code varchar(100) not null
);

alter table bank
    add constraint bank_pkey primary key (id);

alter table bank
    add constraint bank_unique_code unique (code);

-- BANK ACCOUNT
create table bank_account (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    id_bank varchar(36) not null,
    account_number varchar(100) not null,
    account_name varchar(100) not null,
    branch_name varchar(100)
);

alter table bank_account
    add constraint bank_account_pkey primary key (id);

alter table bank_account
    add constraint bank_account_unique_account_number unique (account_number);

alter table bank_account
    add constraint fk_bank_account_bank foreign key (id_bank) references bank (id);

-- RUNNING NUMBER
create table running_number (
    id varchar(36) not null,
    prefix varchar(100) not null,
    last_number bigint not null
);

alter table running_number
    add constraint running_number_pkey primary key (id);

-- CUSTOMER
create table customer (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    code varchar(100) not null,
    name varchar(255) not null,
    email varchar(100) not null,
    mobile_phone varchar(30) not null
);

alter table customer
    add constraint customer_pkey primary key (id);

-- INVOICE TYPE
create table invoice_type (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    code varchar(100) not null,
    name varchar(100) not null,
    payment_type varchar(50) not null
);

alter table invoice_type
    add constraint invoice_type_pkey primary key (id);

-- INVOICE
create table invoice (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    amount decimal(19,2) not null,
    total_payment decimal(19,2) not null,
    description varchar(255) not null,
    due_date date not null,
    paid bit(1) not null,
    invoice_number varchar(100) not null,
    payment_status varchar(50) not null,
    id_invoice_type varchar(36) not null,
    id_customer varchar(36) not null
);

alter table invoice
    add constraint invoice_pkey primary key (id);

alter table invoice
    add constraint invoice_unique_number unique (invoice_number);

alter table invoice
    add constraint fk_invoice_invoice_type foreign key (id_invoice_type) references invoice_type (id);

alter table invoice add constraint fk_invoice_customer foreign key (id_customer) references customer (id);


-- INVOICE DETAIL
create table invoice_detail (
    id varchar(36) not null,
    id_invoice varchar(36) not null,
    product_code varchar(100) not null,
    product_name varchar(255) not null,
    measurement_unit varchar(36) not null,
    unit_price decimal(19,2) not null,
    quantity decimal(19,2) not null
);

alter table invoice_detail
    add constraint invoice_detail_pkey primary key (id);

alter table invoice_detail
    add constraint fk_invoice_detail_invoice foreign key (id_invoice) references invoice (id);

-- PAYMENT PROVIDER
create table payment_provider (
    id varchar(36) not null,
    code varchar(100) not null,
    name varchar(100) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    logo varchar(255) default null
);

alter table payment_provider
    add constraint payment_provider_pkey primary key (id);

alter table payment_provider
    add constraint payment_provider_unique_code unique (code);

-- VIRTUAL ACCOUNT CONFIGURATION
create table virtual_account_configuration (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    code varchar(100) not null,
    name varchar(100) not null,
    id_bank_account varchar(36) not null,
    id_payment_provider varchar(36) not null,
    transaction_fee_flat decimal(19,2) not null,
    transaction_fee_percentage decimal(7,4) not null,
    company_prefix varchar(100) not null,
    account_number_length integer not null
);

alter table virtual_account_configuration
    add constraint virtual_account_configuration_pkey primary key (id);

alter table virtual_account_configuration
    add constraint virtual_accunt_configuration_unique_code unique (code);

alter table virtual_account_configuration
    add constraint virtual_account_configuration_unique_company_prefix unique (company_prefix);

alter table virtual_account_configuration
    add constraint fk_virtual_account_configuration_bank_account foreign key (id_bank_account) references bank_account (id);

alter table virtual_account_configuration
    add constraint fk_virtual_account_configuration_payment_provider foreign key (id_payment_provider) references payment_provider (id);

-- VIRTUAL ACCOUNT
create table virtual_account (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    account_number varchar(255) not null,
    company_id varchar(255),
    payment_type varchar(255) not null,
    id_invoice varchar(36) not null,
    id_payment_provider varchar(36),
    id_virtual_account_configuration varchar(36) not null
);

alter table virtual_account
    add constraint virtual_account_pkey primary key (id);

alter table virtual_account
    add constraint virtual_account_unique_account_number unique (account_number);

alter table virtual_account
    add constraint fk_virtual_account_invoice foreign key (id_invoice) references invoice (id);

alter table virtual_account
    add constraint fk_virtual_account_payment_provider foreign key (id_payment_provider) references payment_provider (id);

alter table virtual_account
    add constraint fk_virtual_account_virtual_account_configuration foreign key (id_virtual_account_configuration) references virtual_account_configuration (id);

-- PAYMENT
create table payment (
    id varchar(36) not null,
    created datetime default null,
    created_by varchar(255) default null,
    status_record varchar(255) not null,
    updated datetime default null,
    updated_by varchar(255) default null,
    amount decimal(19,2) not null,
    transaction_fee decimal(19,2) not null,
    provider_reference varchar(255) not null,
    transaction_time datetime not null,
    id_virtual_account varchar(255) not null
);

alter table payment
    add constraint payment_pkey primary key (id);
alter table payment
    add constraint fk_payment_virtual_account foreign key (id_virtual_account) references virtual_account (id);

-- INVOICE TYPE CONFIGURATION
create table invoice_type_configuration (
    id_invoice_type varchar(36) not null,
    id_virtual_account_configuration varchar(36) not null
);

alter table invoice_type_configuration
    add constraint invoice_type_provider_pkey primary key (id_invoice_type, id_virtual_account_configuration);

alter table invoice_type_configuration
    add constraint fk_invoice_type_provider_type foreign key (id_invoice_type) references invoice_type (id);

alter table invoice_type_configuration
    add constraint fk_invoice_type_provider_configuration foreign key (id_virtual_account_configuration) references virtual_account_configuration (id);

