package com.artivisi.invoice.controller;


import com.artivisi.invoice.dto.CreateInvoiceRequestDto;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;

import static io.restassured.RestAssured.*;

// dia akan menjalankan aplikasi Spring Bootnya dengan PORT yand Random
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest
@Sql(scripts = {
        "/sql/delete-all-data.sql",
        "/sql/insert-sample-data-invoice.sql"
})
public class InvoiceApiControllerTest {
    // sebenarnya juga bisa inject port, kalau ga perlu juga gapapa
    @LocalServerPort private Integer serverPort;

    // Mock itu bikin object palsu, dia test terhadap object tersebut
    // Integration Test, artinya Database nya ada, aplikasi jalan, kita test terhapad aplikasi yang jalan, makanya bisanya tidak menggunakan Mock

    // kita setup dulu untuk REST ASSURED

    @BeforeEach
    void setUp() {
        baseURI = "http://localhost";
        port = serverPort;
        System.out.println("Port: " + serverPort);
    }

    @Test
    void createInvoice() {
        // body nya bisa kita bikin dto
        CreateInvoiceRequestDto invoiceRequestDto = new CreateInvoiceRequestDto();
        invoiceRequestDto.setCustomerCode("081234567890");
        invoiceRequestDto.setInvoiceTypeCode("REG-001");
        invoiceRequestDto.setAmount(new BigDecimal(123000));
        invoiceRequestDto.setDescription("Pendaftaran Kursus");

        with().body(invoiceRequestDto).contentType(ContentType.JSON)
                .when().post("/api/invoice")
        // setelah kita post, selanutnya kita mengharapkan sesuatu
                .then()
                .statusCode(200);
    }
}
