package com.artivisi.invoice.service.impl;

import com.artivisi.invoice.entity.*;
import com.artivisi.invoice.exception.PaymentExceedInvoiceAmountException;
import com.artivisi.invoice.exception.VirtualAccountAlreadyPaidException;
import com.artivisi.invoice.exception.VirtualAccountNotFoundException;
import com.artivisi.invoice.repository.CustomerRepository;
import com.artivisi.invoice.repository.InvoiceTypeRepository;
import com.artivisi.invoice.repository.VirtualAccountConfigurationRepository;
import com.artivisi.invoice.service.impl.PaymentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Sql(scripts = {
        "/sql/delete-all-data.sql",
        "/sql/insert-sample-data-invoice.sql"
})
class PaymentServiceImplTest {

    @Autowired private VirtualAccountConfigurationRepository virtualAccountConfigurationRepository;
    @Autowired private PaymentServiceImpl paymentService;
    @Autowired private CustomerRepository customerRepository;
    @Autowired private InvoiceTypeRepository invoiceTypeRepository;
    @Autowired private InvoiceServiceImpl invoiceService;

    private Invoice invoice;

    @BeforeEach
    public void prepareInvoice() {
        Customer customer = customerRepository.findById("c001").get();
        InvoiceType registrasi = invoiceTypeRepository.findById("registrasi").get();
        BigDecimal amount = new BigDecimal("123000.98");
        String description = "Tagihan Registrasi";

        invoice = invoiceService.createInvoice(
                customer, registrasi, description, amount
        );

    }

    @Test
    void tesPay() throws VirtualAccountNotFoundException, VirtualAccountAlreadyPaidException, PaymentExceedInvoiceAmountException {

        VirtualAccountConfiguration virtualAccountConfiguration = virtualAccountConfigurationRepository.findById("va-bni").get();

        // misal kita udah tau datanya, kita langsung set idnya saja
        paymentService.pay(
                virtualAccountConfiguration.getPaymentProvider(),
                virtualAccountConfiguration.getCompanyPrefix(),
                virtualAccountConfiguration.getCompanyPrefix() + "081234567890",
                new BigDecimal(123000.98),
                "abcd");
    }
}