package com.artivisi.invoice.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RunningNumberServiceImplTest {

    @Autowired private RunningNumberServiceImpl runningNumberService;

    @Test
    void getNumber() {
        Long hasil = runningNumberService.getNumber("Test");
        Assertions.assertNotNull(hasil);
        System.out.println("Hasil = " + hasil);
    }

    // ini akan jadi problem jika kita jalankan dengan multithreading
    // artinya kalo banyak request dan diakses banyak thread
    // Kenapa terjadi data duplikat? Ini adalah masalah Database Locking
    // Itu adalah problem yang umum terjadi jika kita menjalankan multithreading
    // untuk mengatasinya, database itu menyediakan berbagai settingan untuk locking, untuk mengatur lalu lintas untuk banyak user


    @Test
    void getNumberMultiThreaded() throws InterruptedException {
        // bikin threadnya misal 10
        int jumlahThread = 10;
        final int iterasi = 5;

        // kita tampung
        ConcurrentHashMap hasilMap = new ConcurrentHashMap();

        for (int i = 0; i < jumlahThread; i++) {
            Thread thread = new Thread(){
                @Override
                public void run() {
                    try {
                        long delay = Math.abs(new Random().nextInt(100));
                        Thread.sleep(delay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // masing-masig thread iterasinya adalah 5 kali
                    for (int j = 0; j < iterasi; j++) {
                        // ini akan ambil listnya
                        List<Long> lastNumbers = (List<Long>) hasilMap.get(this.getId());
                        // kalo belum ada, maka kita instance kan
                        if (lastNumbers == null) {
                            lastNumbers = new ArrayList<>();
                        }
                        Long hasil = runningNumberService.getNumber("Test");
                        System.out.println("Thread ["+this.getId()+"] last = " + hasil);
                        lastNumbers.add(hasil);
                        hasilMap.put(this.getId(), lastNumbers);
                    }
                }
            };
            thread.start();
        }
        Thread.sleep(10 * 1000);
        Enumeration<Long> keys = hasilMap.keys();
        while (keys.hasMoreElements()) {
            Long key = keys.nextElement();
            System.out.println("==== Thread " + key + " ====");
            System.out.println(hasilMap.get(key));
        }
    }
}