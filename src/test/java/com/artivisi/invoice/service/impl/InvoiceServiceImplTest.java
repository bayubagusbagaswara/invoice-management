package com.artivisi.invoice.service.impl;

import com.artivisi.invoice.entity.*;
import com.artivisi.invoice.repository.CustomerRepository;
import com.artivisi.invoice.repository.InvoiceTypeRepository;
import com.artivisi.invoice.repository.VirtualAccountRepository;
import com.artivisi.invoice.service.InvoiceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

/**
 * tujuannya test ini sebenarnya untuk Client Code
 * jadi bisa mengetahui kode program yang akan memanggil method-method di service nya
 * misal kode program yang memanggil createInvoice
 */
@SpringBootTest
@Sql(scripts = {
        "/sql/delete-all-data.sql",
        "/sql/insert-sample-data-invoice.sql"
})
class InvoiceServiceImplTest {

    @Autowired private CustomerRepository customerRepository;
    @Autowired private InvoiceService invoiceService;
    @Autowired private InvoiceTypeRepository invoiceTypeRepository;
    @Autowired private VirtualAccountRepository virtualAccountRepository;

    @Test
    void createInvoice() {
        Customer customer = customerRepository.findById("c001").get();
        InvoiceType invoiceType = invoiceTypeRepository.findById("registrasi").get();
        BigDecimal amount = new BigDecimal("123000.98");
        String description = "Tagihan Registrasi";

        Invoice invoice = invoiceService.createInvoice(customer, invoiceType, description, amount);
//        VirtualAccountConfiguration virtualAccountConfiguration = new VirtualAccountConfiguration();
//        virtualAccountConfiguration.setId("va-bni");
//        VirtualAccount virtualAccount = new VirtualAccount();
//        virtualAccount.setInvoice(invoice);
//        virtualAccount.setAccountNumber("12345");
//        virtualAccount.setVirtualAccountConfiguration(virtualAccountConfiguration);
//        virtualAccount.setCreated(LocalDateTime.now());
//        virtualAccount.setCreatedBy("Test");
//        virtualAccountRepository.save(virtualAccount);


//        System.out.println(invoice);
//        System.out.println("Invoice type: " + invoice.getInvoiceType());
//        System.out.println("Invoice type configuration: " + invoice.getInvoiceType().getInvoiceTypeConfiguration());

        assertNotNull(invoice);
        assertNotNull(invoice.getInvoiceNumber());
        System.out.println("Invoice number : " + invoice.getInvoiceNumber());

        Iterable<VirtualAccount> vaList = virtualAccountRepository.findByInvoice(invoice);
        int countVa = 0;
        for (VirtualAccount va : vaList) {
            countVa++;
            System.out.println("No VA : " + va.getAccountNumber());
        }
        assertEquals(2, countVa); // hasilnya 2, karena kita insert 2 data

    }

}