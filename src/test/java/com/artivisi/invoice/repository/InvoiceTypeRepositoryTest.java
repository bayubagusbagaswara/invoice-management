package com.artivisi.invoice.repository;

import com.artivisi.invoice.entity.InvoiceType;
import com.artivisi.invoice.entity.PaymentType;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
// karena tiap kita test data yang dikirimkan sama dan apabila kita ubah maka akan error
// oleh itu kita butuh sql, yang akan dijalankan setiap kalo dia mau test
// kita taruh script sql nya di file properties
// sehingga saat kita mau test, data yang dikirim selalu data baru (data lama akan dihapus dahulu)
// karena kita pertama kita melakukan script delete kemudian insert
@Sql(scripts = {
        "/sql/delete-all-data.sql",
        "/sql/insert-inactive-invoice-type.sql"
})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InvoiceTypeRepositoryTest {

    @Autowired
    InvoiceTypeRepository invoiceTypeRepository;

    @Test
    @Order(1)
    @DisplayName("Create Invoice Type")
    void testInsertInvoiceType() throws InterruptedException {

        // buat objectnya
        InvoiceType invoiceType = new InvoiceType();

        // set objectnya
        invoiceType.setCode("IT-001");
        invoiceType.setName("Invoice Type Test");

        // assertNull, artinya harapannya adalah null datanya karena emang datanya belum di insert
        Assertions.assertNull(invoiceType.getId());

        // lalu simpan invoiceType, secara otomatis akan generate id
        invoiceTypeRepository.save(invoiceType);

        // tampilkan data hasil setelah insert
        System.out.println("ID : " + invoiceType.getId());
        System.out.println("Create Time : " + invoiceType.getCreated());
        System.out.println("Create by : " + invoiceType.getCreatedBy());
        System.out.println("Update Time : " + invoiceType.getUpdated());
        System.out.println("Update by : " + invoiceType.getUpdatedBy());
        System.out.println("Status Record : " + invoiceType.getStatusRecord());

        // assertNotNull, artinya harapannya adalah not null karena datanya sudah berhasil di insert
        Assertions.assertNotNull(invoiceType.getId());
        Assertions.assertNotNull(invoiceType.getCreated());
        Assertions.assertNotNull(invoiceType.getCreatedBy());
        Assertions.assertNotNull(invoiceType.getUpdated());
        Assertions.assertNotNull(invoiceType.getUpdatedBy());
        Assertions.assertNotNull(invoiceType.getStatusRecord());

        // cek equals, harusnya sama karena created dan update bersamaan
        Assertions.assertEquals(invoiceType.getCreated(), invoiceType.getUpdated());

        // tunda
        Thread.sleep(1000);

        // update
        invoiceType.setName("Test Update");

        // kita save, artinya balikan dari setelah save adalah menimpa invoiceType dengan data baru
        // makanya perlu kita simpan/teriman balikannya kedalam  variabel invoiceType
        invoiceType = invoiceTypeRepository.save(invoiceType);

        // jadi variable invoiceType disini sudah dalam keadaan diupdate (data baru)
        System.out.println("Create Time : " + invoiceType.getCreated());
        System.out.println("Update Time : " + invoiceType.getUpdated());

        // cek equals, harusnya tidak sama, karena jeda 1 detik lalu kita update
        Assertions.assertNotEquals(invoiceType.getCreated(), invoiceType.getUpdated());

    }

    @Test
    @Order(2)
    @DisplayName("Insert Data Active and Inactive")
    void testQuerySoftDelete() {
        // insert 2 data dengan status ACTIVE dan INACTIVE
        // jika Soft Delete berhasil, maka hasil record data adalah 1, karena yang 1 lagi statusnya INACTIVE
        Long jumlahRecord = invoiceTypeRepository.count();
        System.out.println("Jumlah record : " + jumlahRecord);
        Assertions.assertEquals(1, jumlahRecord);

    }

    /**
     * Jadi Kegunaan SoftDelete ini adalah menghapus data di database, tapi tanpa menghapus total datanya
     * hanya mengubah salah satu key, yakni Status Record menjadi ACTIVE atau INACTIVE
     * hasilnya data tetap masih disimpan di database
     */
    @Test
    @Order(3)
    @DisplayName("Delete Data using Soft Delete")
    void testSoftDelete() {
        InvoiceType invoiceType = invoiceTypeRepository.findById("test002").get();
        invoiceTypeRepository.delete(invoiceType);
        Long jumlahRecord = invoiceTypeRepository.count();
        System.out.println("Jumlah record : " + jumlahRecord);
        // expected 0, karena datanya berhasil dihapus (atau di INACTIVE menggunakan SOFT DELETE)
        Assertions.assertEquals(0, jumlahRecord);

    }
}