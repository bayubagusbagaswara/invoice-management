
insert into bank (id, code, name, central_bank_code, status_record, created, created_by)
values ('bca', 'BCA', 'Bank Central Asia', '014', 'ACTIVE', current_timestamp, 'TEST');

insert into bank (id, code, name, central_bank_code, status_record, created, created_by)
values ('bsi', 'BSI', 'Bank Syariah Indonesia', '451', 'ACTIVE', current_timestamp, 'TEST');

insert into bank (id, code, name, central_bank_code, status_record, created, created_by)
values ('bni', 'BNI', 'Bank Nasional Indonesia', '009', 'ACTIVE', current_timestamp, 'TEST');

insert into bank (id, code, name, central_bank_code, status_record, created, created_by)
values ('cimb', 'CIMB', 'CIMB Niaga', '022', 'ACTIVE', current_timestamp, 'TEST');

insert into bank_account (id, id_bank, account_number, account_name, branch_name, status_record, created, created_by)
values ('bca001', 'bca', '123456789001', 'Test Account BCA', 'Kantor Pusat', 'ACTIVE', current_timestamp, 'TEST');

insert into bank_account (id, id_bank, account_number, account_name, branch_name, status_record, created, created_by)
values ('bsi001', 'bsi', '123456789002', 'Test Account BSI', 'Kantor Pusat', 'ACTIVE', current_timestamp, 'TEST');

insert into bank_account (id, id_bank, account_number, account_name, branch_name, status_record, created, created_by)
values ('bni001', 'bni', '123456789003', 'Test Account BNI', 'Kantor Pusat', 'ACTIVE', current_timestamp, 'TEST');

insert into bank_account (id, id_bank, account_number, account_name, branch_name, status_record, created, created_by)
values ('cimb001', 'cimb', '123456789004', 'Test Account CIMB', 'Kantor Pusat', 'ACTIVE', current_timestamp, 'TEST');


insert into payment_provider (id, code, name, status_record, created, created_by)
values ('bankbca', 'BCA', 'Bank BCA', 'ACTIVE', current_timestamp, 'TEST');

insert into payment_provider (id, code, name, status_record, created, created_by)
values ('bankbni', 'BNI', 'Bank BNI', 'ACTIVE', current_timestamp, 'TEST');

insert into payment_provider (id, code, name, status_record, created, created_by)
values ('bankbsi', 'BSI', 'Bank BSI', 'ACTIVE', current_timestamp, 'TEST');

insert into payment_provider (id, code, name, status_record, created, created_by)
values ('bankcimb', 'CIMB', 'Bank CIMB', 'ACTIVE', current_timestamp, 'TEST');

insert into payment_provider (id, code, name, status_record, created, created_by)
values ('ovo', 'OVO', 'Ovo', 'ACTIVE', current_timestamp, 'TEST');

insert into payment_provider (id, code, name, status_record, created, created_by)
values ('gopay', 'GOPAY', 'GoPay', 'ACTIVE', current_timestamp, 'TEST');


insert into virtual_account_configuration (id, code, name, id_payment_provider, id_bank_account, transaction_fee_flat, transaction_fee_percentage, company_prefix, account_number_length, status_record, created, created_by)
values('va-bni', 'VA-BNI', 'Virtual Account BNI', 'bankbni', 'bni001', 2000, 0.0, '0123', 12, 'ACTIVE', current_timestamp, 'TEST');

insert into virtual_account_configuration (id, code, name, id_payment_provider, id_bank_account, transaction_fee_flat, transaction_fee_percentage, company_prefix, account_number_length, status_record, created, created_by)
values('va-gopay', 'VA-GOPAY', 'Pembayaran Gopay', 'bankbca', 'bca001', 0, 0.025, '0987', 12, 'ACTIVE', current_timestamp, 'TEST');


insert into invoice_type (id, code, name, payment_type, status_record, created, created_by)
values ('registrasi', 'REG-001', 'Biaya Pendaftaran', 'CLOSED', 'ACTIVE', current_timestamp, 'TEST');

insert into invoice_type (id, code, name, payment_type, status_record, created, created_by)
values ('donasi', 'DONASI-001', 'Sumbangan Sukarela', 'OPEN', 'ACTIVE', current_timestamp, 'TEST');

insert into invoice_type (id, code, name, payment_type, status_record, created, created_by)
values ('uang-muka', 'DP-001', 'Uang Muka', 'INSTALLMENT', 'ACTIVE', current_timestamp, 'TEST');


insert into invoice_type_configuration (id_invoice_type, id_virtual_account_configuration)
values ('registrasi', 'va-bni');

insert into invoice_type_configuration (id_invoice_type, id_virtual_account_configuration)
values ('registrasi', 'va-gopay');

insert into invoice_type_configuration (id_invoice_type, id_virtual_account_configuration)
values ('donasi', 'va-gopay');

insert into invoice_type_configuration (id_invoice_type, id_virtual_account_configuration)
values ('uang-muka', 'va-bni');


insert into customer (id, code, name, email, mobile_phone, status_record, created, created_by)
values ('c001', '081234567890', 'Customer 001', 'c001@gmail.com', '081234567890', 'ACTIVE', current_timestamp, 'TEST');

