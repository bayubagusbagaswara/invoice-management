insert into invoice_type (id, code, name, payment_type, created, created_by, updated, updated_by, status_record )
values ('test001', 'T-001', 'Test 001', 'OPEN', '2021-08-13T20:05:57.867728800', 'Test User', '2021-08-13T20:05:57.867728800', 'Test User', 'INACTIVE');

insert into invoice_type (id, code, name, payment_type, created, created_by, updated, updated_by, status_record )
values ('test002', 'T-002', 'Test 002', 'CLOSED', '2021-08-13T20:05:57.867728800', 'Test User', '2021-08-13T20:05:57.867728800', 'Test User', 'ACTIVE');