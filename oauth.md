# OAuth
- versi OAuth 2.1

- 1st party = Resource owner/user password. Contoh antar product Google (masih dalam lingkup google)
- 3rd party = dia bisa baca username/password aplikasi kita
- sehingga saat dia request lagi, akhirnya tidak aman

- biasanya pembuat Aplikasi (kita) tidak mengijinkan third party untuk mendapatkan resource owner/user password
- Cuma masalahnya tetap bisa bikin login form
- dan user tidak paham, bahwa ini aplikasi third party (langsung masukin email+passwordnya)
- jadi third party rentan terhadap phishing (website palsu)
- nanti bisa disalahgunakan username+password kita
- sehingga di OAuth, password itu dihapuskan (biar tidak dipake pihak ketiga)
- Jadi di OAuth 2.1 hanya tinggal Authorization Code + PKCE, Client Credentials, Device Grant


## Grant type (flow nya)
### Auth Code + PKCE
- jaman sekarang berlaku untuk aplikasi web (web server side)
- seperti Java, PHP, Node JS, Ruby, dsb
- ini juga berlaku untuk aplikasi mobile (ios dan andorid)
- ini untuk aplikasi web client side (angular, vue, react, jquery)

- Flownya mulai dari User
- User ingin mengakses aplikasi dari depan (Client Side)
- kemudian datanya user ada di belakang (Server side)
- ada 4 pihak yang terlibat dalam flow

1. User atau Resource Owner (kita sebagai pemiliki Resourde file-file di database)
2. Client Side aplikasi
3. Authorization Server
4. Server Side aplikasi
   - Database atau Resource
   - Resource Server (untuk melayani akses resource database)

- Flow Aslinya
- si client ingin mengakses datanya sendiri melalui resource server lalu ke resource 
- lalu resource akan dibalikin dikirim kedepan
- user->client app->resource server->resource database
- Tapi gimana caranya biar berjalan dengan Secure?
Langkah
1. User mengirimkan request awal (tanpa credential, tidak bawa token dsb) ke Client App
2. Client app akan mengecek dulu, apakah dia sudah authenticated apa belum? Jika client app tidak mengenali, maka  Client App akan mengembalikan ke user dengan menyuruh User untuk Login terlebih dahulu. Dilain sisi Client App akan redirect ke auth server
Yang dibawa saat redirect, adalah client ID (IDnya aplikasi, karena tidak semua aplikasi terdaftar OAuth)
kemudian kita sebutkan Grant Type nya apa, kemudian kita mengharapkan Response nya apa 
karena kita menggunakan PKCE, maka kita kirimkan Code Challenge (parameter tambahannya adalah algoritmanya untuk verifikasi)
PKCE
- akan mengenerate random string (untuk verifikasi)
- lalu kan di hash SHA256 (untuk challenge)
3. Melakukan authentication oleh Auth Server
Auth Server akan menyimpan Code Challenge
4. Setelah selesai authenticated, maka dia balikan ke User atau redirect,
kemudian dia akan memberikan Authorization code 
5. User balik lagi ke CLient App, dan membawa authorization code
6. dia akan bawa auth code, dan kirim verifier
7. di Auth server, dia akan mengecek auth code dan verifier
verifier ini akan dicompare dengan verifier saat pertama kali dibuat (saat proses nomor 3)
8. Jika compare berhasil (matching), maka Auth server akan mengembalikan ke Client App. Biasanya namanya Access Token dan Refresh Token
9. Lalu Client App akan akses ke Resource Server dengan membawa Access Token
10. Resource Server akan mengecek verifikasi Access Token, apakah valid atau tidak
11. sad
5xxzcc