# API Authentication

- User akan login menggunakan browser
- lalu browser akan ke Aplikasi Web kita
- untuk memastikan User nya benar atau salah kita menggunakan Username + Password ketika login
- Tujuannya mendapatkan Username adalah agar bisa diasosiasi (dicocokan) dengan Session ID tiap request oleh User yang berbeda-beda
- Lantas bagaimana jika Aplikasi Web kita ingin diakses oleh Aplikasi lain (bukan html browser), misal Aplikasi Mobile

## Aplikasi Lain mengakses Aplikasi Web kita
- biasanya balikan data dari Aplikasi Web nya adalah Datanya saja
- datanya bisa berformat JSON, XML, CSV
- Bagaimana auhentication dari Aplikasi luar?
- caranya menggunakan method authentication dengan HTTP

## HTTP Auth
1. Basic Auth 
2. Digest
3. Form Based (yang biasa kita pake, tampilkan HTML form, lalu user isi, data diterima di server, dicek, session id nya kita relasikan dengan username)

### Basic Auth
- biasanya kita tambahkan di `Header`
- di Header kita masukkan username + password
- lalu kita encode menggunakan Base64 (encoding), tujuan encode agar bisa dikirim lewat network, menjadi string yang aman untuk dikirim
- misal di Postman ada menu Header
- di Header ada pilihan Authorization
- Base64 itu akan dibawa terus untuk tiap kali request. Server akan ngecek dia bawa Basic Auth nya apa tidak, kalau tidak maka server akan menolak
- 401 : belum authenticate, atau auth tidak dibawa
### Digest
- username + password kita pake untuk generate sesuatu
- biasanya menggunakan Hashing (searah), setelah username + paswword dijadikan Digest
- maka Digest tidak bisa lagi dikonversi lagi menjadi username + password