# Aplikasi Invoice Management #

Aplikasi ini dipakai untuk mengelola invoice dan menyambungkan dengan berbagai metode pembayaran masa kini.

Diantara metode pembayaran yang akan disupport antara lain:

* Virtual Account Bank
    * Bank BNI
    * Bank CIMB
    * Bank BSI

* e-Wallet
    * Ovo
    * GoPay

* QR Payment
    * QRIS

Tipe tagihan yang tersedia:

    * CLOSED : bayar sesuai nominal. Kalau tidak sesuai, maka akan ditolak
    * OPEN : pembayaran berapapun diterima
    * INSTALLMENT : pembayaran diterima selama total akumulasi lebih kecil atau sama dengan nilai tagihan  

Fitur aplikasi :

* Manajemen Customer
  
  * Registrasi Customer
  * Rekap tagihan Customer
  * Histori pembayaran

* Manajemen invoice
  
  * Membuat invoice
  * Mengganti nilai dan tanggal jatuh tempo
  * Membatalkan invoice
  

    

## Setup Database ##

### PostgreSQL ###


```
docker run --rm \
--name invoice-db \
  -e POSTGRES_DB=invoicedb \
  -e POSTGRES_USER=invoice \
  -e POSTGRES_PASSWORD= QCm2nF1Dll8BNaQpowZB \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v "$PWD/invoicedb-data:/var/lib/postgresql/data" \
  -p 5432:5432 \
postgres:13
```
** password database lebih baik menggunakan String Random

### MySQL ###
```
DATABASE_PORT = 3306
DATABASE_NAME = invoicedb
DATABASE_USERNAME = root
DATABASE_PASSWORD = 

spring.datasource.url=jdbc:mysql://localhost:3306/<nama_database>?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=
```
