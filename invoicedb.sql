-- MariaDB dump 10.19  Distrib 10.4.20-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: invoicedb
-- ------------------------------------------------------
-- Server version	10.4.20-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `flyway_schema_history`
--

DROP TABLE IF EXISTS `flyway_schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flyway_schema_history`
--

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;
INSERT INTO `flyway_schema_history` VALUES (1,'20210809','Skema Awal','SQL','V20210809__Skema_Awal.sql',-229542652,'root','2021-08-09 13:47:50',240,1);
/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id` varchar(36) NOT NULL,
  `invoice_number` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status_record` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `amount` decimal(19,2) NOT NULL,
  `description` varchar(255) NOT NULL,
  `due_date` date NOT NULL,
  `paid` bit(1) NOT NULL,
  `id_invoice_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoice_unique_number` (`invoice_number`),
  KEY `FKco4sbxv9cj2oevm6cdpq76ffb` (`id_invoice_type`),
  CONSTRAINT `FKco4sbxv9cj2oevm6cdpq76ffb` FOREIGN KEY (`id_invoice_type`) REFERENCES `invoice_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_type`
--

DROP TABLE IF EXISTS `invoice_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_type` (
  `id` varchar(36) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status_record` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_type`
--

LOCK TABLES `invoice_type` WRITE;
/*!40000 ALTER TABLE `invoice_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_type_provider`
--

DROP TABLE IF EXISTS `invoice_type_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_type_provider` (
  `id_invoice_type` varchar(255) NOT NULL,
  `id_payment_provider` varchar(255) NOT NULL,
  PRIMARY KEY (`id_invoice_type`,`id_payment_provider`),
  KEY `FKdue5pwwyn091emo8u1e5fkh7k` (`id_payment_provider`),
  CONSTRAINT `FKdue5pwwyn091emo8u1e5fkh7k` FOREIGN KEY (`id_payment_provider`) REFERENCES `payment_provider` (`id`),
  CONSTRAINT `FKfsym0hpqp4d8llimqvtn1iih1` FOREIGN KEY (`id_invoice_type`) REFERENCES `invoice_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_type_provider`
--

LOCK TABLES `invoice_type_provider` WRITE;
/*!40000 ALTER TABLE `invoice_type_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_type_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` varchar(36) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status_record` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `amount` decimal(19,2) NOT NULL,
  `provider_reference` varchar(255) NOT NULL,
  `transaction_time` datetime NOT NULL,
  `id_virtual_account` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKptriq88d7e8io9mhri8p10cq0` (`id_virtual_account`),
  CONSTRAINT `FKptriq88d7e8io9mhri8p10cq0` FOREIGN KEY (`id_virtual_account`) REFERENCES `virtual_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_provider`
--

DROP TABLE IF EXISTS `payment_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_provider` (
  `id` varchar(36) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status_record` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payment_provider_unique_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_provider`
--

LOCK TABLES `payment_provider` WRITE;
/*!40000 ALTER TABLE `payment_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `virtual_account`
--

DROP TABLE IF EXISTS `virtual_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `virtual_account` (
  `id` varchar(36) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status_record` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `virtual_account_type` varchar(255) NOT NULL,
  `id_invoice` varchar(255) NOT NULL,
  `id_payment_provider` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbbdwdxpgdisiikyyhf2xteblc` (`id_invoice`),
  KEY `FKt3t7f64hvgk4xjblsovqqkpll` (`id_payment_provider`),
  CONSTRAINT `FKbbdwdxpgdisiikyyhf2xteblc` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`id`),
  CONSTRAINT `FKt3t7f64hvgk4xjblsovqqkpll` FOREIGN KEY (`id_payment_provider`) REFERENCES `payment_provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `virtual_account`
--

LOCK TABLES `virtual_account` WRITE;
/*!40000 ALTER TABLE `virtual_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `virtual_account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-09 20:53:37
