# Konsep Authentication

## Authentication
- berkaitan dengan siapa usernya?


## Konsep Login di Aplikasi Web
- ini berlaku untuk semua aplikasi website
- Pada intinya protocol HTTP itu sebetulnya `Stateless`
- Stateless itu artinya dia tidak bisa membedakan antara request satu dengan request yang lain
- Cara membedakannya adalah si aplikasi saat pertama kali dia diakses, misal akses dengan GET /transaksi
- maka dia akan kirim comment ke browser (sisi client)
- Jadi dari Aplikasi kita (sisi Server) saat pertama dia akses, maka dia akan ngirim sebuah comment ke Browser (sisi client)
- commentnya berfungsi untuk memasang `Session ID`
- Session ID ini digenerate random saja (algoritmanya ada macem-macem)
- Kemudian dia akan memasang session id ditempatnya User/Client di browser maupun di mobile
- Bagaimana caranya? ada beberapa cara
- saat browser melakukan GET, dia akan membawa session id, jadi urlnya misalnya GET /transaksi?sid=123 atau GET /transaksi?sid=4456
- gimana caranya agar session ID terbawa oleh requestnya?
- maka dia harus menyimpan data session id nya di sisi User
- ada beberapa cara untuk menyimpan session id disisi user di storage
- Cara 1: dia akan set yang namanya `Cookie`
- Kapan dia set Cookie? pada waktu dia pertama kali usernya akses ke Aplikasi Web
- Jadi di browser nya akan menyimpan Cookie
- Cara 2: dia akan embed di variable URL
- pada waktu dia bikin URL misal <a href="/transaksi">Transaksi</a>
- lalu kita ubah diserver melalui koding <a href="/transaksi?sid=12345>Transaksi</a>
- jadi session id nya kita masukkan ke url
- Cara 3: kita masukkan kedalam Form
- <input type="hidden" name="sid" value="1234">
- sehingga input itu akan terbawa ke server

## Cookie
- cookie ini akan dikirim melalui request header
- kita bisa ambil di request headernya
- session id aja belum cukup untuk mengidentifikasi user, karena dia akan hanya random dan unik
- bagaimana cara mengasosiasi bahwa session ini adalah User yang mana?
- si aplikasi harus mengecek session id nya itu siapa
- dia (Aplikasi server) kemudian tampilkan login form 
- karena pada login, User harus memasukkan username & password, lalu di submit
- pada waktu submit tentunya dia akan menambahkan session id
- kemudian di sisi server akan dicek username & password 
- pada waktu dia sukses, misalnya username & passwordnya benar
- maka setelah itu dia akan mengasosiakan atau menghubungkan bahwa session id ini adalah User A misalnya (kalo username password benar)
- Ada table Session ID yang menyimpan session idnya berapa, dan expired nya kapan, dan usernya siapa
- kalo framework spring security sudah ada yang ngurusin
- kita setting juga Session Concurrency (max session per user), karena biasanya satu user akan mengakes dua browser yang berbeda (session idna berbeda, padahal user sama)
- setting juga Priority, adalah siapa yang menang, siapa yang duluan login, jika login kedua maka ditolak. Atau sebalikanya yang belakang akan menang

## Session ID menggunakan DATA - contoh: JWT (Json Web Token)
- token tapi ada datanya
- datanya apa aja?
- Biasanya datanya adalah usernya siapa? expirednya kapan? scope? issuernya siapa?
- sehingga dia (user) tidak nanya-nanya lagi ke server

## Authorization
- berkaitan dengan permission atau ijin akses

- misal aplikasi kita punya fitur input transaksi dan approve transaksi
- lalu kita ingin membedakan permissionnya
- misal yang bisa input transaksi adalah staff
- dan yang isa approe transaksi adalah manager
- Nah gimana cara mengimplementasikan seperti itu?
- sebenarnya tidak ada standar yang baku
- yang paling primitive/sederhana kita mapping antara tabel User dan Permission

1. Skema Permission 1 membuat table User dan table Permission
2. Skema Permission 2 menambakan sebuah table namanya Role jadi ada 3, yakni User, Role, Permission
- si usernya ini nanti punya Role, relasinya 1 Role memiliki banyak User, baru si Role memiliki banyak Permission (Many To Many)