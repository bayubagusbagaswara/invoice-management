## Transaction Propagation ##

adalah transaksi database biasa, cuma disediakan fitur oleh Java supaya mudah.

1. Pertama ada fitur yang namanya Transaction Management
- ini adalah fitur sejak Java EE yang namanya J2EE
- di J2EE ada fitur CMT (Container Managed Transaction)
  - Zaman dulu aplikasi server pake yang besar: Weblogic, Websphere (dia punya fitur koneksi management semacam connection pooling tapi deklarasi didalam aplikasi server).
  - si aplikasi server tsb ingin memisahkan role antar Operation dan Development
    - karena connection sudah dimanage oleh si Container, maka kita bisa menyuruh container tsb melakukan transaction juga (menjalankan perintah AutoCommit false/true)
- CMT digunakan jaman dulu karena ingin menciptakan XA transaction
- Apa XA? misal kita punya aplikasi
- dan aplikasi kita butuh konek ke 2 database 
- tapi itu mengakibatkan performance makin menurun, makanya tidak populer saat ini
- Akhirnya yang saat ini populer dan banyak digunakan ya kita manage sendiri yang namanya adalah Compensating Transaction (SAGA)
- si CMT ini cara kodingnya (khusus Java)
- ada 2 cara: 
- 1. Programmatic (kita bikin kode program). Kita lookup dulu (atau lihat ke aplikasi server, siap atau belum database)
    - disini kita setting connectionnya ke TransactionManager
    - ini kurang efisien
- 2. Declarative, caranya pake annotation. Kita declare apa yang kita mau.